﻿//using _62A_PropertyWebsite.Filters;
using System.Web;
using System.Web.Mvc;

namespace _62A_PropertyWebsite
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new LogAttribute());    // every action will be logged!
            filters.Add(new HandleErrorAttribute());
        }
    }
}
