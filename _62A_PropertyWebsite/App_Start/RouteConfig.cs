﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace _62A_PropertyWebsite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "CustomSearch",
                url: "{propertyType}-in-{locality}",
                defaults: new { controller = "Search", action = "Search" }
                );

            /*

            // listingsearch/{locality}/{action}
            // e.g. ~/listingsearch/sliema/search 
            routes.MapRoute(
                name: "ListingSearch",                      // name
                url: "listingsearch/{locality}/{action}",   // url pattern
                defaults: new { controller="Search", action="Search" }       // default values when information is missing
            );
            */

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


            /*
            routes.MapRoute(
                name: "Search",
                url: "Luxury-Property-in-{locality}",
                defaults: new { controller = "Search", action = "LuxurySearch" }
            );

            routes.MapRoute(
                name: "LocalisationDefaultDefault",
                url: "{culture}/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}
