﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _62A_PropertyWebsite.Models
{
    public class RealEstateDBContext : DbContext
    {
        public RealEstateDBContext() : base("RealEstateDBContext")
        {
        }

        public DbSet<PropertyModel> Properties { get; set; }
    }
}