﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using _62A_PropertyWebsite.Models;

namespace _62A_PropertyWebsite.Controllers
{
    public class PropertyModelsController : Controller
    {
        private RealEstateDBContext db = new RealEstateDBContext();

        public ActionResult Test()
        {
            IList<PropertyModel> model = new List<PropertyModel>();
            model.Add(
                   new PropertyModel(
                       1,
                       "Paola",
                       "Maisonette",
                       2,
                       "Sale",
                       "A very nice maisonette",
                       130,
                       180000.0M));

            model.Add(
                   new PropertyModel(
                       2,
                       "Floarian",
                       "Maisonette",
                       2,
                       "To Let",
                       "A very nice maisonette",
                       130,
                       180000.0M));

            model.Add(
                   new PropertyModel(
                       3,
                       "Hamrun",
                       "Maisonette",
                       2,
                       "Sale",
                       "A very nice maisonette",
                       130,
                       180000.0M));

            IEnumerable<PropertyModel> modelToReturnToView = (IEnumerable<PropertyModel>)model;

            ViewBag.TitleText = "The title for the Test Page!"; // You might obtain this information from the Database
            

            return View(modelToReturnToView);
        }

        // GET: PropertyModels
        public ActionResult Index()
        {
            return View(db.Properties.ToList());
        }

        // GET: PropertyModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropertyModel propertyModel = db.Properties.Find(id);
            if (propertyModel == null)
            {
                return HttpNotFound();
            }
            return View(propertyModel);
        }

        // GET: PropertyModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PropertyModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Reference,Locality,PropertyType,Bedrooms,ContractType,Description,FloorArea,Price,ImageURL")] PropertyModel propertyModel)
        {
            if (ModelState.IsValid)
            {
                db.Properties.Add(propertyModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(propertyModel);
        }

        // GET: PropertyModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropertyModel propertyModel = db.Properties.Find(id);
            if (propertyModel == null)
            {
                return HttpNotFound();
            }
            return View(propertyModel);
        }

        // POST: PropertyModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Reference,Locality,PropertyType,Bedrooms,ContractType,Description,FloorArea,Price,ImageURL")] PropertyModel propertyModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(propertyModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(propertyModel);
        }

        // GET: PropertyModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropertyModel propertyModel = db.Properties.Find(id);
            if (propertyModel == null)
            {
                return HttpNotFound();
            }
            return View(propertyModel);
        }

        // POST: PropertyModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PropertyModel propertyModel = db.Properties.Find(id);
            db.Properties.Remove(propertyModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
