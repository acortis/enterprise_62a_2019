﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62A_PropertyWebsite.Controllers
{
    public class HomeController : Controller
    {
        //[Log]
        public ActionResult Index()
        {
            return View();
        }

        //[Log]
        public ViewResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public FilePathResult GetImage()
        {
            return File(@"~\Images\listing.jpg", "image/jpg");
        }

        public JsonResult GetJsonInfo()
        {
            // Preferably not over GET requests!
            // JsonRequestBehavior.DenyGet
            return Json("MyModel", JsonRequestBehavior.AllowGet);
        }

        public ActionResult RedirectMe()
        {
            return Redirect("www.google.com");
        }
    }
}