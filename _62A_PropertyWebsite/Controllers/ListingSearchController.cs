﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62A_PropertyWebsite.Controllers
{
    [RoutePrefix("Search")]
    public class ListingSearchController : Controller
    {
        // GET: ListingSearch
        [Route("Property/{id?}")]
        public ContentResult Search(String id)
        {
            return Content("This url was handled by the default route and sent here!");
        }

        [Route("50PercentPriceReduced")]
        public ActionResult PriceReduced()
        {
            return Content("Price reduced");
        }
    }
}