﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62A_PropertyWebsite.Controllers
{
    public class SearchController : Controller
    {
        // GET: Index
        public ActionResult Index(String locality)
        {
            return Content("This is the Search Controller, " +
                "Action is Index and parameter locality has a value of " + locality);
        }

        public ActionResult Search(String locality)
        {
            return Content("This is the Search Controller, " +
                "Action is Seach and parameter locality has a value of " + locality);
        }

        public ActionResult MostViewed(String locality)
        {
            return Content("This is the Search Controller, " +
                "Action is Most Viewed and parameter locality has a value of " + locality);
        }
    }
}