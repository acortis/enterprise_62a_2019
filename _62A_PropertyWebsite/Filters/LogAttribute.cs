﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _62A_PropertyWebsite.Filters
{
    public class LogAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            String userIdentityName = filterContext.HttpContext.User.Identity.Name;
            String requestPathAndQuery = filterContext.HttpContext.Request.Url.PathAndQuery;

            String logInfo = "Some new information...";

            IList<String> log;

            HttpContext.Current.Application.Lock(); // only this thread can access the code below alone

            try     // If we have an exception
            {
                log = LoadListFromApplicationVariable("Log");

                // log is a reference to the application variable
                // we can edit it!
                log.Add(logInfo);
            }
            finally // the finally block will still execute
            {
                HttpContext.Current.Application.UnLock();   // once we are ready, we allow other threads to use the same code
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            int responseStatusCode = filterContext.HttpContext.Response.StatusCode;

            String logInfo = "Some new information...";

            IList<String> log;

            HttpContext.Current.Application.Lock(); // only this thread can access the code below alone

            try     // If we have an exception
            {
                log = LoadListFromApplicationVariable("Log");

                // log is a reference to the application variable
                // we can edit it!
                log.Add(logInfo);
            }
            finally // the finally block will still execute
            {
                HttpContext.Current.Application.UnLock();   // once we are ready, we allow other threads to use the same code
            }

            base.OnResultExecuted(filterContext);
        }

        private static IList<string> LoadListFromApplicationVariable(String variableName)
        {
            IList<string> log;
            log = (IList<String>)HttpContext.Current.Application[variableName];    // load the log from the application variable

            if (log == null)    // this is the first time that we are loading the log and the log does not exist yet...
            {
                // so we create it!
                log = new List<String>();
                HttpContext.Current.Application[variableName] = log;
            }

            return log;
        }
    }
}