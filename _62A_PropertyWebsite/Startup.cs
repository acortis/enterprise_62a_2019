﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_62A_PropertyWebsite.Startup))]
namespace _62A_PropertyWebsite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
